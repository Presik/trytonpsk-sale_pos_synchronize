from trytond.pool import PoolMeta


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.method.selection.extend([
                ('api.log|synchronize_pull',
                    "Synchronize Pull"),
                ('api.log|synchronize_push',
                    "Synchronize Push")])
