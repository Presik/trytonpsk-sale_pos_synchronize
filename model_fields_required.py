models_fields_required = {
    'party.party': [
        'name', 'id_number', 'addresses.street', 'addresses.party',
        'contact_mechanisms.type', 'contact_mechanisms.value', 'contact_mechanisms.party',
        'customer_payment_term', 'write_date', 'create_date',
        'regime_tax', 'type_document', 'type_person', 'fiscal_regimen',
        'account_receivable', 'account_payable', 'invoice_type'],
    'product.category': [
        'name', 'parent',
    ],
    'product.template': [
        'name', 'code', 'write_date', 'reference', 'salable', 'create_date',
        'consumable', 'purchasable', 'producible', 'account_category', 'type',
        'sale_uom', 'default_uom', 'purchase_uom', 'cost_price_method',
        'list_price', 'active', 'sale_price_w_tax', 'products.template', 'products.active',
        'products.code', 'products.description', 'products.short_name', 'products.extra_tax',
        'products.barcode', 'products.tag', 'products.cost_price'],
    'product.price_list': ['name', 'company', 'tax_included', 'unit'],
    'product.price_list.line': ['price_list', 'category', 'product', 'quantity', 'formula'],
    'company.employee': [
        'party', 'active', 'code'
    ],
    'account.voucher.paymode': [
        'account', 'bank_account', 'journal', 'kind', 'payment_type', 'sequence_multipayment',
        'sequence_payment', 'sequence_receipt', 'name', 'payment_means_code', 'party', 'require_party',
    ],
    'account.account': [
        'parent', 'type', 'code', 'closed', 'bank_reconcile',  'reconcile', 'name'
    ],
    'product.category': [
        'name', 'accounting', 'customer_taxes_used', 'supplier_taxes_used', 'active',
        'account_revenue', 'account_expense', 'account_stock', 'account_cogs'
    ],
    'party.consumer': [
        'name', 'phone', 'address', 'id_number',
        'birthday', 'delivery', 'notes'
    ],
    'sale.sale': [
    ],
    'account.statement': [

    ],
    'account.statement.line': [
        
    ],

}
