from trytond.pool import Pool
import requests
import json
from datetime import date
from trytond.protocols.jsonrpc import JSONEncoder, JSONDecoder


HEADERS = {
           'Accept': 'application/json',
           'Content-type': 'application/json'
        }


def encoder(obj):
    # FIXME: Add datetime, bytes, decimal, too
    if isinstance(obj, date):
        return {
            '__class__': 'date',
            'year': obj.year,
            'month': obj.month,
            'day': obj.day,
        }
    else:
        json.JSONEncoder.default(str(obj))
    return obj


class Proxy(object):
    # __name__ = 'proxy.sychronize'
    def __init__(self):
        pool = Pool()
        Configuration = pool.get('sale.configuration')
        configuration = Configuration(1)
        self.url_request = configuration.uri
        print(self.url_request)

    def search(self, data):
        uri = self.url_request + '/search'
        data = json.dumps(data)
        response = requests.post(uri, data=data, headers=HEADERS)
        return response

    def create(self, data):
        uri = self.url_request + '/create'
        data = json.dumps(data, cls=JSONEncoder, separators=(',', ':'))
        response = requests.post(uri, data=data, headers=HEADERS)
        return response

    def write(self, data):
        uri = self.url_request + '/write'
        data = json.dumps(data)
        response = requests.post(uri, data=data, headers=HEADERS)
        return response

    def method_instance(self, data):
        uri = self.url_request + '/method_instance'
        data = json.dumps(data)
        response = requests.post(uri, data=data, headers=HEADERS)
        return response

    def button_method(self, data):
        uri = self.url_request + '/button_method'
        data = json.dumps(data)
        response = requests.post(uri, data=data, headers=HEADERS)
        return response
