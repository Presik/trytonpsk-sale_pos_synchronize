from sql import Null, Table
from trytond.transaction import Transaction
from trytond.model import fields
from trytond.pool import PoolMeta


class Account(metaclass=PoolMeta):
    __name__ = 'account.account'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(Account, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        account = Table(cls._table)
        cursor.execute(
            *account.select(
                account.id, account.external_id,
                where=account.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *account.update(
                    [account.external_id], [account.id],
                    where=account.external_id == Null))
