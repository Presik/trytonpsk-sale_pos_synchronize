# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
# from datetime import date, datetime
from decimal import Decimal
# from trytond.exceptions import UserError
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval
from sql import Null, Table
from trytond.transaction import Transaction
# from trytond.wizard import (
#     Wizard, StateTransition, StateView, Button, StateReport
# )
# from trytond.report import Report
# from .exceptions import ValidationSaleInvoice
# from trytond.i18n import gettext

_ZERO = Decimal('0.0')


def _round(self, value, digits=2):
    return Decimal(str(round(float(value)), digits))


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    external_id = fields.Integer('External Id')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        # cls.invoice_type.selection.extend([('1E', 'Venta Electronica Offline')])

    @classmethod
    def __register__(cls, module_name):
        super(Sale, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        sale = Table(cls._table)
        cursor.execute(
            *sale.select(
                sale.id, sale.external_id,
                where=sale.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *sale.update(
                    [sale.external_id], [sale.id],
                    where=sale.external_id == Null))


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    external_id = fields.Integer('External Id')

    @classmethod
    def __setup__(cls):
        super(SaleLine, cls).__setup__()

    @classmethod
    def __register__(cls, module_name):
        super(SaleLine, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        saleline = Table(cls._table)
        cursor.execute(
            *saleline.select(
                saleline.id, saleline.external_id,
                where=saleline.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *saleline.update(
                    [saleline.external_id], [saleline.id],
                    where=saleline.external_id == Null))


class Statement(metaclass=PoolMeta):
    __name__ = 'account.statement'

    external_id = fields.Integer('External Id')

    @classmethod
    def __setup__(cls):
        super(Statement, cls).__setup__()

    @classmethod
    def __register__(cls, module_name):
        super(Statement, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        statement = Table(cls._table)
        cursor.execute(
            *statement.select(
                statement.id, statement.external_id,
                where=statement.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *statement.update(
                    [statement.external_id], [statement.id],
                    where=statement.external_id == Null))


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'

    external_id = fields.Integer('External Id')

    @classmethod
    def __setup__(cls):
        super(StatementLine, cls).__setup__()

    @classmethod
    def __register__(cls, module_name):
        super(StatementLine, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        statementline = Table(cls._table)
        cursor.execute(
            *statementline.select(
                statementline.id, statementline.external_id,
                where=statementline.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *statementline.update(
                    [statementline.external_id], [statementline.id],
                    where=statementline.external_id == Null))
