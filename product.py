from trytond.model import fields
from trytond.pool import PoolMeta
from sql import Null, Table
from trytond.transaction import Transaction


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(Template, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        template = Table(cls._table)
        cursor.execute(
            *template.select(
                template.id, template.external_id,
                where=template.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *template.update(
                    [template.external_id], [template.id],
                    where=template.external_id == Null))


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(Product, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        product = Table(cls._table)
        cursor.execute(
            *product.select(
                product.id, product.external_id,
                where=product.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *product.update(
                    [product.external_id], [product.id],
                    where=product.external_id == Null))


class ProductCategory(metaclass=PoolMeta):
    __name__ = 'product.category'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(ProductCategory, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        category = Table(cls._table)
        cursor.execute(
            *category.select(
                category.id, category.external_id,
                where=category.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *category.update(
                    [category.external_id], [category.id],
                    where=category.external_id == Null))


class PriceList(metaclass=PoolMeta):
    __name__ = 'product.price_list'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(PriceList, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        pricelist = Table(cls._table)
        cursor.execute(
            *pricelist.select(
                pricelist.id, pricelist.external_id,
                where=pricelist.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *pricelist.update(
                    [pricelist.external_id], [pricelist.id],
                    where=pricelist.external_id == Null))


class PriceListLine(metaclass=PoolMeta):
    __name__ = 'product.price_list.line'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(PriceListLine, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        listline = Table(cls._table)
        cursor.execute(
            *listline.select(
                listline.id, listline.external_id,
                where=listline.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *listline.update(
                    [listline.external_id], [listline.id],
                    where=listline.external_id == Null))
