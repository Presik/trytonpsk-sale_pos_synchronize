from trytond.model import fields
from trytond.pool import PoolMeta
from sql import Null, Table
from trytond.transaction import Transaction


class VoucherPayMode(metaclass=PoolMeta):
    'Voucher Pay Mode'
    __name__ = 'account.voucher.paymode'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(VoucherPayMode, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        pay_mode = Table(cls._table)
        cursor.execute(
            *pay_mode.select(
                pay_mode.id, pay_mode.external_id,
                where=pay_mode.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *pay_mode.update(
                    [pay_mode.external_id], [pay_mode.id],
                    where=pay_mode.external_id == Null))   
