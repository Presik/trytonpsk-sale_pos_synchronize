# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import synchronization
from . import ir
from . import party
from . import product
from . import account
from . import voucher
from . import invoice


def register():
    Pool.register(
        sale.Sale,
        sale.SaleLine,
        synchronization.Configuration,
        synchronization.ApiLog,
        ir.Cron,
        party.Party,
        party.ContactMechanism,
        party.Address,
        party.Employee,
        party.Consumer,
        product.Product,
        product.Template,
        product.ProductCategory,
        product.PriceList,
        product.PriceListLine,
        account.Account,
        voucher.VoucherPayMode,
        sale.Statement,
        sale.StatementLine,
        invoice.Invoice,
        invoice.InvoiceLine,
        module='sale_pos_synchronize', type_='model')
    Pool.register(
        module='sale_pos_synchronize', type_='wizard')
    Pool.register(
        module='sale_pos_synchronize', type_='report')
