import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)
from .model_fields_required import models_fields_required
from datetime import date, datetime, timedelta
from decimal import Decimal
from .proxy import Proxy
from trytond.modules.product import price_digits, round_price


APPLICATIONS = [("synchronize", "Synchronize")]


def round_value(n, digits):
    print(n, float(str(round(float(n), digits))), digits)
    return str(round(float(n), digits))


class Configuration(metaclass=PoolMeta):
    __name__ = 'sale.configuration'
    uri = fields.Char('Uri To Sinchronize', help='http://domain:port/db')


class ApiLog(metaclass=PoolMeta):
    "Api Log"
    __name__ = 'api.log'

    type = fields.Selection([
        ('log', 'Log'),
        ('in', 'In'),
        ('out', 'Out'),
    ], 'Type Synchronization')

    @classmethod
    def __setup__(cls):
        super(ApiLog, cls).__setup__()
        cls.application.selection.extend(APPLICATIONS)

    def default_state():
        return 'pending'

    @classmethod
    def synchronize_pull(cls):
        proxy = Proxy()
        pool = Pool()
        Date = pool.get('ir.date')
        records = cls.search([
            ('application', '=', 'synchronize'),
            ('status', '=', 'done'),
            ('type', '=', 'in')
        ], limit=1, order=[('create_date', 'DESC')])

        if records:
            create_date_log = records[0].create_date
        else:
            create_date_log = datetime.now() - timedelta(days=30)

        value = {
            'status': 'error',
            'type': 'in',
            'application': 'synchronize',
            'record_date': Date.today()
        }
        # log, = cls.create([value])
        domain = [
             'OR',
            ('write_date', '>=', str(create_date_log)),
            ('create_date', '>=', str(create_date_log))
        ]
        models = [
            'account.account', 'party.party', 'product.category',
            'product.template', 'product.price_list',
            'product.price_list.line', 'account.voucher.paymode',
            'company.employee', 'party.consumer']
        # models = ['account.account', 'party.party']
        for model in models:
            Model_ = pool.get(model)
            fields = models_fields_required[model]
            context = Transaction().context
            data = {
                'model': model,
                'context': context,
                'domain': domain,
                'fields_names': fields,
            }
            response = proxy.search(data)
            if response.status_code == 200:
                model_function = 'process_' + model.replace('.', '_')
                process_function = getattr(cls, model_function)
                process_function(response.json(), Model_, value)
            log_dict = copy.deepcopy(value)
            log_dict['model_name'] = model
            log_dict['status'] = 'processed'
            cls.create([log_dict])
            # log.state = 'done'
            # log.save()

    @classmethod
    def process_party_party(cls, args, Model_, value):
        pool = Pool()
        Address = pool.get('party.address')
        Contact = pool.get('party.contact_mechanism')

        for rec in args:
            parties = Model_.search([('external_id', '=', rec['id'])])
            contacts = rec.pop('contact_mechanisms.')
            addresses = rec.pop('addresses.')
            if not parties:
                rec['external_id'] = rec.pop('id')
                for c in contacts:
                    c['external_id'] = c.pop('id')
                for a in addresses:
                    a['external_id'] = a.pop('id')
                rec['contact_mechanisms'] = [('create', contacts)]
                rec['addresses'] = [('create', addresses)]
                try:
                    Model_.create([rec])
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al crear tercero ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for create', rec)
            else:
                try:
                    Model_.write(parties, rec)
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al escribir tercero ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for write', rec)

                for a in addresses:
                    external_id = a.pop('id')
                    try:
                        address, = Address.search([('external_id', '=', external_id)])
                        a['party'] = address.party.id
                        Address.write([address], a)
                    except:
                        a['external_id'] = external_id
                        a['party'] = parties[0].id
                        Address.create([a])

                for c in contacts:
                    external_id = c.pop('id')
                    try:
                        contact, = Contact.search([('external_id', '=', external_id)])
                        c['party'] = contact.party.id
                        Contact.write([contact], c)
                    except:
                        c['external_id'] = external_id
                        c['party'] = parties[0].id
                        Contact.create([c])

    @classmethod
    def process_product_template(cls, args, Model_, value):
        pool = Pool()
        Product = pool.get('product.product')
        for rec in args:
            templates = Model_.search([('external_id', '=', rec['id'])])
            products = rec.pop('products.')

            rec['list_price'] = round_price(Decimal(rec['list_price']))
            rec['sale_price_w_tax'] = round_price(Decimal(rec['sale_price_w_tax']))

            for p in products:
                p['cost_price'] = round_price(Decimal(p['cost_price']))

            if not templates:
                rec['external_id'] = rec.pop('id')
                for p in products:
                    p['external_id'] = p.pop('id')
                rec['products'] = [('create', products)]
                try:
                    Model_.create([rec])
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al crear producto template ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for create', rec)
            else:
                try:
                    Model_.write(templates, rec)
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al escribir producto template ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for write', rec)

                for p in products:
                    external_id = p.pop('id')
                    try:
                        product, = Product.search([('external_id', '=', external_id)])
                        p['template'] = product.template
                        Product.write([product], p)
                    except Exception as e:
                        p['external_id'] = external_id
                        p['template'] = templates[0].id
                        Product.create([p])

    @classmethod
    def process_company_employee(cls, args, Model_, value):
        for rec in args:
            employees = Model_.search([('external_id', '=', rec['id'])])
            Party = Pool().get('party.party')
            try:
                party, = Party.search(['external_id', '=', rec['party']])
                rec['party'] = party.id
            except:
                log_dict = copy.deepcopy(value)
                log_dict['request_json'] = rec
                log_dict['msg_response'] = 'Error al crear empleado, tercero no existe'
                cls.create([log_dict])
                continue

            if not employees:
                rec['external_id'] = rec.pop('id')
                try:
                    Model_.create([rec])
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al crear empleado ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for create', rec)
            else:
                try:
                    Model_.write(employees, rec)
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al escribir empleado ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for write', rec)

    @classmethod
    def process_account_account(cls, args, Model_, value):
        for rec in args:
            accounts = Model_.search([('external_id', '=', rec['id'])])
            external_parent = rec.get('parent')
            parent = Model_.search([('external_id', '=', external_parent)])

            if not parent:
                account_p = [a for a in args if a['id'] == external_parent]
                cls.process_account_account([account_p], Model_, value)

            if not accounts:
                rec['external_id'] = rec.pop('id')
                rec['parent'] = parent.id
                try:
                    Model_.create([rec])
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al crear cuenta contable ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for create', rec)
            else:
                try:
                    Model_.write(accounts, rec)
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al escribir cuenta contable ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for write', rec)

    @classmethod
    def process_account_voucher_paymode(cls, args, Model_, value):
        for rec in args:
            pay_modes = Model_.search([('external_id', '=', rec['id'])])

            if not pay_modes:
                rec['external_id'] = rec.pop('id')
                try:
                    Model_.create([rec])
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al crear modo de pago ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for create', rec)
            else:
                try:
                    Model_.write(pay_modes, rec)
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al escribir modo de pago ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for write', rec)

    @classmethod
    def process_party_consumer(cls, args, Model_, value):
        for rec in args:
            consumers = Model_.search([('external_id', '=', rec['id'])])

            if not consumers:
                rec['external_id'] = rec.pop('id')
                try:
                    Model_.create([rec])
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al crear consumidor ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for create', rec)
            else:
                try:
                    Model_.write(consumers, rec)
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al escribir consumidor ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for write', rec)

    @classmethod
    def process_product_category(cls, args, Model_, value):
        for rec in args:
            categories = Model_.search([('external_id', '=', rec['id'])])
            customer_taxes = rec.pop('customer_taxes_used')
            supplier_taxes = rec.pop('supplier_taxes_used')

            if not categories:
                rec['external_id'] = rec.pop('id')
                if customer_taxes:
                    rec['customer_taxes'] = [('add', customer_taxes)]

                if supplier_taxes:
                    rec['supplier_taxes'] = [('add', supplier_taxes)]

                try:
                    Model_.create([rec])
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al crear categoria de producto ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for create', rec)

            else:
                try:
                    if customer_taxes or supplier_taxes:
                        Model_.write(categories, {
                            'customer_taxes': [('remove', [t.id for t in categories[0].customer_taxes])],
                            'supplier_taxes': [('remove', [t.id for t in categories[0].supplier_taxes])]
                            })
                        Model_.write(categories, {
                            'customer_taxes': [('add', customer_taxes)],
                            'supplier_taxes': [('add', supplier_taxes)]
                            })
                    Model_.write(categories, rec)
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al escribir categoria de producto ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for write', rec)

    @classmethod
    def process_product_price_list(cls, args, Model_, value):
        for rec in args:
            price_lists = Model_.search([('external_id', '=', rec['id'])])

            if not price_lists:
                try:
                    Model_.create([rec])
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al crear lista de precios ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for create', rec)

            else:
                try:

                    Model_.write(price_lists, rec)
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al escribir lista de precios ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for write', rec)

    @classmethod
    def process_product_price_list_line(cls, args, Model_, value):
        pool = Pool()
        Product = pool.get('product.product')
        Category = pool.get('product.category')
        PriceList = pool.get('product.price_list')
        for rec in args:
            lines = Model_.search([('external_id', '=', rec['id'])])

            price_lists = PriceList.search([('external_id', '=', rec['price_list'])])
            if not price_lists:
                print('Error no se encontro la lista de precios')
                continue
            rec['price_list'] = price_lists[0].id

            category = None
            if rec['category']:
                try:
                    category, = Category.search(['external_id', '=', rec['category']])
                except:
                    continue

            product = None
            if rec['product']:
                try:
                    product, = Product.search(['external_id', '=', rec['product']])
                except:
                    continue

            rec['category'] = category.id if category else None
            rec['product'] = product.id if product else None
            if not lines:
                try:
                    Model_.create([rec])
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al crear linea de lista de precios ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for create', rec)

            else:
                try:

                    Model_.write(lines, rec)
                except Exception as e:
                    log_dict = copy.deepcopy(value)
                    log_dict['request_json'] = rec
                    log_dict['msg_response'] = 'Error al escribir la linea de lista de precios ' + str(e)
                    cls.create([log_dict])
                    print(e, 'for write', rec)

    @classmethod
    def synchronize_push(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        value = {
            'status': 'processed',
            'type': 'in',
            'record_date': Date.today()
        }
        context = Transaction().context
        context['user'] = Transaction().user
        models = [
            'sale.sale', 'account.invoice',
            'account.statement', 'account.statement.line']
        for model in models:
            Model_ = pool.get(model)
            model_function = 'process_' + model.replace('.', '_')
            process_function = getattr(cls, model_function)
            process_function(Model_, model, value, context)

    @classmethod
    def process_sale_sale(cls,  Model_, model_name, value, context):
        proxy = Proxy()
        rec = cls.search([
            ('model_name', '=', model_name),
            ('status', '=', 'processed'),
            ])
        domain = [
            'OR',
            [
                ('state', 'in', ['processing', 'done']),
                ('external_id', '=', None)
            ],
        ]
        if rec:
            domain.append([
                ('create_date', '>=', str(rec.create_date))
            ])
            domain.append([('write_date', '>=', str(rec.create_date))])
        sales = Model_.search(domain)
        print(context, 'context')
        for sale in sales:
            if sale.external_id:
                values = {
                    'turn': sale.turn,
                    'shop': sale.shop.id,
                    'salesman': sale.salesman.external_id if sale.salesman else None,
                    'sale_device': sale.sale_device.id,
                    'delivery_time': sale.delivery_time,
                    'order_status': sale.order_status,
                    'shipping_time': sale.shipping_time,
                    }
                response = proxy.write({
                    'model': model_name,
                    'ids': [sale.external_id],
                    'values': values,
                    'context': context})

                if response.status_code == 200:
                    print(response.json())
                else:
                    log_dict = copy.deepcopy(value)
                    log_dict['status'] = 'error'
                    log_dict['model_name'] = model_name
                    log_dict['msg_response'] = 'Error al escribir venta ' + response.text
                    cls.create([log_dict])
            else:
                sale_ = {
                    # 'number': sale.number,
                    'party': sale.party.external_id,
                    'invoice_party': sale.invoice_party.external_id if sale.invoice_party else sale.party.external_id,
                    'invoice_type': sale.invoice_type,
                    'price_list': sale.price_list.external_id,
                    'payment_term': sale.payment_term.id,
                    'company': sale.company.party.external_id,
                    'comment': sale.comment,
                    'sale_date': date.strftime(sale.sale_date, "%Y-%m-%d"),
                    'sale_device': sale.sale_device.id,
                    'reference': sale.number,
                    'warehouse': sale.warehouse.id,
                    'state': sale.state,
                    'salesman': sale.salesman.external_id if sale.salesman else None,
                    'shop': sale.shop.id,
                    'turn': sale.turn,
                    'source': sale.source.id,
                    'consumer': sale.consumer.external_id if sale.consumer else None,
                    'table_assigned': sale.table_assigned.id if sale.table_assigned else None,
                    'delivery_time': sale.delivery_time,
                    'order_status': sale.order_status,
                    'shipping_time': sale.shipping_time,
                }
                response = proxy.create({
                    'model': model_name,
                    'record': sale_,
                    'context': context
                })
                if response.status_code == 200:
                    result = response.json()
                    sale.external_id = result[0]
                    sale.save()
                    model = 'sale.line'
                    for line in sale.lines:
                        line_ = {
                            'product': line.product.external_id,
                            'base_price': round_value(line.base_price, 4),
                            'unit_price': round_value(line.unit_price, 4),
                            'unit': line.unit.id,
                            'quantity': line.quantity,
                            'sale': sale.external_id,
                            'taxes': [('add', [t.id for t in line.taxes])]
                        }
                        response = proxy.create({
                            'model': model,
                            'record': line_,
                            'context': context
                            })
                        if response.status_code == 200:
                            result = response.json()
                            line.external_id = result[0]
                            line.save()
                        else:
                            log_dict = copy.deepcopy(value)
                            log_dict['status'] = 'error'
                            log_dict['msg_response'] = 'Error al crear linea de venta '  + sale.number + ' ' + response.text
                            cls.create([log_dict])
                    proxy.button_method({
                        'method': 'store_cache',
                        'model': model_name,
                        'ids': [sale.external_id],
                        'context': context,
                    })
                    proxy.button_method({
                        'method': 'do_stock_moves',
                        'model': model_name,
                        'ids': [sale.external_id],
                        'context': context,
                    })
                else:
                    log_dict = copy.deepcopy(value)
                    log_dict['status'] = 'error'
                    log_dict['msg_response'] = 'Error al crear venta ' + sale.number + ' ' + response.text
                    cls.create([log_dict])

    @classmethod
    def process_account_invoice(cls,  Model_, model_name, value, context):
        proxy = Proxy()
        rec = cls.search([
            ('model_name', '=', model_name),
            ('status', '=', 'processed'),
            ])

        create_date = str(datetime.now() - timedelta(days=10))
        if rec:
            create_date = str(rec.create_date)

        domain = [
            'OR',
            [
                ('create_date', '>=', create_date),
                ('external_id', '=', None),
                ('type', '=', 'out')

            ], [
                ('write_date', '>=', create_date),
                ('type', '=', 'out')
            ]
        ]

        invoices = Model_.search(domain)
        for invoice in invoices:
            if invoice.external_id:
                values = {
                    'turn': invoice.turn,
                    'shop': invoice.shop.id,
                    'salesman': invoice.salesman.external_id if invoice.salesman else None,
                    }
                response = proxy.write({
                    'model': model_name,
                    'ids': [invoice.external_id],
                    'values': values,
                    'context': context})

                if response.status_code == 200:
                    print(response.json())
                else:
                    log_dict = copy.deepcopy(value)
                    log_dict['status'] = 'error'
                    log_dict['model_name'] = model_name
                    log_dict['msg_response'] = 'Error al escribir venta ' + response.text
                    cls.create([log_dict])
            else:
                invoice_ = {
                    'number': invoice.number,
                    'party': invoice.party.external_id,
                    'invoice_address': invoice.invoice_address.external_id,
                    'account': invoice.account.external_id,
                    'journal': invoice.journal.id,
                    'type': 'out',
                    'invoice_type': invoice.invoice_type,
                    'payment_term': invoice.payment_term.id,
                    'company': invoice.company.party.external_id,
                    'comment': invoice.comment,
                    'invoice_date': date.strftime(invoice.invoice_date, "%Y-%m-%d"),
                    'reference': invoice.reference,
                    'state': 'draft',
                    'salesman': invoice.salesman.external_id if invoice.salesman else None,
                    'shop': invoice.shop.id,
                    'turn': invoice.turn,
                }
                response = proxy.create({
                    'model': model_name,
                    'record': invoice_,
                    'context': context
                })
                if response.status_code == 200:
                    result = response.json()
                    invoice.external_id = result[0]
                    invoice.save()
                    model = 'account.invoice.line'
                    for line in invoice.lines:
                        line_ = {
                            'product': line.product.external_id,
                            'unit_price': round_value(line.unit_price, 4),
                            'unit': line.unit.id,
                            'quantity': line.quantity,
                            'invoice': invoice.external_id,
                            'origin': line.origin.__name__ + ',' + str(line.origin.external_id),
                            'account': line.account.external_id,
                            'taxes': [('add', [t.id for t in line.taxes])],
                        }
                        if line.base_price:
                            line_['base_price'] = round_value(line.base_price, 4)

                        response = proxy.create({
                            'model': model,
                            'record': line_,
                            'context': context
                            })
                        if response.status_code == 200:
                            result = response.json()
                            line.external_id = result[0]
                            line.save()
                        else:
                            log_dict = copy.deepcopy(value)
                            log_dict['status'] = 'error'
                            log_dict['msg_response'] = 'Error al crear linea de factura '  + invoice.number + ' ' + response.text
                            cls.create([log_dict])
                    proxy.button_method({
                        'method': 'validate_invoice',
                        'model': model_name,
                        'ids': [invoice.external_id],
                        'context': context,
                    })
                else:
                    log_dict = copy.deepcopy(value)
                    log_dict['status'] = 'error'
                    log_dict['msg_response'] = 'Error al crear factura ' + invoice.number + ' ' + response.text
                    cls.create([log_dict])

    @classmethod
    def process_account_statement(cls, Model_, model_name, value, context):
        proxy = Proxy()
        rec = cls.search([
            ('model_name', '=', model_name),
            ('status', '=', 'processed'),
            ])

        create_date = str(datetime.now() - timedelta(days=10))
        if rec:
            create_date = str(rec.create_date)

        domain = [
            'OR',
            [
                ('create_date', '>=', create_date),
                ('external_id', '=', None),
            ],
            [
                ('write_date', '>=', create_date),
            ]
        ]
        statements = Model_.search(domain)
        for st in statements:
            if st.external_id:
                statement = {
                    'name': st.name,
                    'journal': st.journal.id,
                    'turn': st.turn,
                    'date': date.strftime(st.date, "%Y-%m-%d"),
                    'sale_device': st.sale_device.id,
                    'salesman': st.salesman.id if st.salesman else None,
                    'start_balance': round_value(st.start_balance, 2),
                    'end_balance': round_value(st.end_balance, 2),
                    'state': st.state,
                }
                response = proxy.write({
                    'model': model_name,
                    'ids': [st.external_id],
                    'values': statement,
                    'context': context
                })
                if response.status_code == 200:
                    print('exito....')
                else:
                    log_dict = copy.deepcopy(value)
                    log_dict['status'] = 'error'
                    log_dict['msg_response'] = 'Error al escribir estado de cuenta ' + st.name + ' ' + response.text
                    cls.create([log_dict])
            else:
                statement = {
                    'name': st.name,
                    'journal': st.journal.id,
                    'turn': st.turn,
                    'date': date.strftime(st.date, "%Y-%m-%d"),
                    'sale_device': st.sale_device.id,
                    'salesman': st.salesman.id if st.salesman else None,
                    'end_balance': round_value(st.end_balance, 2),
                    'start_balance': round_value(st.start_balance, 2),
                    'state': st.state,
                    # 'expenses_daily': '',
                    # 'count_money': '',
                }
                response = proxy.create({
                    'model': model_name,
                    'record': statement,
                    'context': context
                })
                if response.status_code == 200:
                    result = response.json()
                    st.external_id = result[0]
                    st.save()
                else:
                    log_dict = copy.deepcopy(value)
                    log_dict['status'] = 'error'
                    log_dict['msg_response'] = 'Error al crear estado de cuenta ' + st.name + ' ' + response.text
                    print(log_dict, 'log_dict')
                    cls.create([log_dict])

    @classmethod
    def process_account_statement_line(cls, Model_, model_name, value, context):
        proxy = Proxy()
        rec = cls.search([
            ('model_name', '=', model_name),
            ('status', '=', 'processed'),
            ])

        create_date = str(datetime.now() - timedelta(days=10))
        if rec:
            create_date = str(rec.create_date)

        domain = [
            'OR',
            [
                ('create_date', '>=', create_date),
                ('external_id', '=', None),
            ],
            [
                ('write_date', '>=', create_date),
            ]
        ]
        lines = Model_.search(domain)
        for line in lines:
            if line.external_id:
                line_ = {
                    'date': date.strftime(line.date, "%Y-%m-%d"),
                    'amount': round_value(line.amount, 2),
                    'account': line.account.external_id,
                    'party': line.party.external_id,
                    'description': line.description,
                    'sale': line.sale.external_id,
                    'invoice': line.invoice.external_id if line.invoice else None,
                }
                response = proxy.write({
                    'model': model_name,
                    'ids': [line.external_id],
                    'values': line_,
                    'context': context
                })
                if response.status_code == 200:
                    print('exito...')
                else:
                    log_dict = copy.deepcopy(value)
                    log_dict['status'] = 'error'
                    log_dict['msg_response'] = 'Error al editar linea de estado de cuenta ' \
                        + line.party.name + ',' + line.amount + ' ' + response.text
                    cls.create([log_dict])
            else:
                line_ = {
                    'date': date.strftime(line.date, "%Y-%m-%d"),
                    'amount': round_value(line.amount, 2),
                    'account': line.account.external_id,
                    'party': line.party.external_id,
                    'description': line.description,
                    'sale': line.sale.external_id,
                    'invoice': line.invoice.external_id if line.invoice else None,
                }
                if line.extra_amount:
                    line_['extra_amount'] = round_value(line.extra, 2),
                if line.net_amount:
                    line_['net_amount'] = round_value(line.net_amount, 2),
                response = proxy.create({
                    'model': model_name,
                    'record': line_,
                    'context': context
                })
                if response.status_code == 200:
                    result = response.json()
                    line.external_id = result[0]
                    line.save()
                else:
                    log_dict = copy.deepcopy(value)
                    log_dict['status'] = 'error'
                    log_dict['msg_response'] = 'Error al crear linea de estado de cuenta ' \
                        + line.party.name + ',' + str(line.amount) + ' ' + response.text
                    cls.create([log_dict])

    # @classmethod
    # def get_sales_to_create(cls, sales):
    #     to_create = []
    #     to_create_append = to_create.append
    #     products_ = set()
    #     parties_ = set()
    #     statements_ = set()
    #     for sale in sales:
    #         parties_.add(sale.party.id_number)
    #         for p in sale.payments:
    #             statements_.add(p.statement.name)
    #         for line in sale.lines:
    #             products_.add(line.product.code)
    #     opts_parties = cls.get_opts_request(
    #             '/search',
    #             'POST',
    #             'party.party',
    #             domain=[('id_number', 'in', parties_)],
    #             fields=['id', 'id_number', 'account_receivable']
    #         )
    #     response = cls.get_response(opts_parties)
    #     parties = None
    #     if response.status_code == 200:
    #         parties = response.json()
    #         parties = {p['id_number']: p for p in parties}
    #     opts_products = cls.get_opts_request(
    #             '/search',
    #             'POST',
    #             'product.product',
    #             domain=[('code', 'in', products_)],
    #             fields=['id', 'code', 'description']
    #         )
    #     response = cls.get_response(opts_products)
    #     products = None
    #     if response.status_code == 200:
    #         products = response.json()
    #         print('products', products)
    #         products = {p['code']: p for p in products}
    #     opts_statements = cls.get_opts_request(
    #             '/search',
    #             'POST',
    #             'account.statement',
    #             domain=[
    #                 ('name', 'in', statements_),
    #                 ('state', '=', 'draft'),
    #                 ],
    #             fields=['id', 'name']
    #         )
    #     response = cls.get_response(opts_statements)
    #     statements = None
    #     if response.status_code == 200:
    #         statements = response.json()
    #         statements = {s['name']: s for s in statements}
    #     for sale in sales:
    #         _sale = cls.create_sale(sale, parties, products, statements)
    #         to_create_append(_sale)
    #     return to_create

    # @classmethod
    # def create_sale(cls, sale, parties, products, statements):
    #     payments = sale.payments
    #     _sale = {
    #         'description': sale.description,
    #         'party': parties[sale.party.id_number]['id'],
    #         'payment_term': sale.payment_term.id,
    #         'company': sale.company.party.id,
    #         'comment': sale.comment,
    #         'sale_date': sale.sale_date,
    #         'reference': sale.number,
    #         'warehouse': sale.warehouse.id,
    #         'state': sale.state,
    #         'invoice_type': sale.invoice_type,
    #         'salesman': sale.salesman.id if sale.salesman else None,
    #         'shop': sale.shop.id,
    #         'lines': cls.get_sale_lines(sale.lines, products),
    #         'payments': cls.get_payment_lines(payments, statements, parties)
    #     }
    #     return _sale

    # @classmethod
    # def get_payment_lines(cls, payments, statements, parties):
    #     payments_to_create = []
    #     payments_to_create_append = payments_to_create.append
    #     for p in payments:
    #         statement = statements[p.statement.name]
    #         party = parties[p.party.id_number]
    #         _line = {
    #             'statement': statement['id'],
    #             'description': p.description,
    #             'date': p.date,
    #             'amount': float(p.amount),
    #             'extra_amount': float(p.extra_amount) if p.extra_amount else None,
    #             'account': party['account_receivable']['id'],
    #             'party': party['id'],
    #         }
    #         payments_to_create_append(_line)
    #     return payments_to_create

    # @classmethod
    # def get_sale_lines(cls, lines, products):
    #     lines_to_create = []
    #     lines_to_create_append = lines_to_create.append
    #     for line in lines:
    #         product = products[line.product.code]
    #         _line = {
    #             'product': product['id'],
    #             'description': product['description'],
    #             'quantity': line.quantity,
    #             'base_price': float(line.base_price),
    #             'discount_rate': float(line.discount_rate),
    #             'unit_price': float(line.unit_price),
    #             'unit': line.unit.id,
    #             'taxes': [('create', [tax.id for tax in line.taxes])]
    #         }
    #         lines_to_create_append(_line)
    #     return lines_to_create
