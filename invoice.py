# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
# from datetime import date, datetime
from decimal import Decimal
# from trytond.exceptions import UserError
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval
from sql import Null, Table
from trytond.transaction import Transaction

_ZERO = Decimal('0.0')


def _round(self, value, digits=2):
    return Decimal(str(round(float(value)), digits))


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    external_id = fields.Integer('External Id')

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls._check_modify_exclude.update(['external_id'])
        # cls.invoice_type.selection.extend([('1E', 'Venta Electronica Offline')])

    @classmethod
    def __register__(cls, module_name):
        super(Invoice, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        invoice = Table(cls._table)
        cursor.execute(
            *invoice.select(
                invoice.id, invoice.external_id,
                where=invoice.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *invoice.update(
                    [invoice.external_id], [invoice.id],
                    where=invoice.external_id == Null))


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    external_id = fields.Integer('External Id')

    @classmethod
    def __setup__(cls):
        super(InvoiceLine, cls).__setup__()
        cls._check_modify_exclude.update(['external_id'])

    @classmethod
    def __register__(cls, module_name):
        super(InvoiceLine, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        invoiceline = Table(cls._table)
        cursor.execute(
            *invoiceline.select(
                invoiceline.id, invoiceline.external_id,
                where=invoiceline.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *invoiceline.update(
                    [invoiceline.external_id], [invoiceline.id],
                    where=invoiceline.external_id == Null))
